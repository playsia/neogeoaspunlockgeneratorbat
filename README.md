NEOGEO Arcade Stick Pro Unlock Key Generator Batch File
=======================================================

This program (well.. batch script) is designed to use the work by cyanic located here:
https://gitlab.com/modmyclassic/neogeo-asp-unlock-generator

The "private" key is in the public and has been published by SNK themselves...
To verify:
* Open the NeoGeo ASP's source code, the file is in qui.rar and is called qcdkeymanager.o
* You can find that here: https://github.com/xudarren/NeogeoASP_OSS 
* Open the file in a text editor and search for: "-----BEGIN RSA PRIVATE KEY-----"

Usage
-----

This quick usage guide assumes you are using windows.

* Obtain the latest release of NEOGEO ASP Unlock Generator (get the latest win-x64)
* This can be found here: https://gitlab.com/modmyclassic/neogeo-asp-unlock-generator/pipelines 
* Extract the release, (the folder inside the .zip will be "win-x64")
* Place the files unlockgenerate.bat and the keys folder inside your new win-x64 folder
* Run unlockgenerate.bat, enter your serial number without hyphens (to see: go to options (cog) on your NEOGEO-ASP, it is abbreviated to "SN")
* If everything runs well (you've placed the files in the correct folder and entered your serial number correctly) the .cdk files will be generated in /keys/cdkey
* Copy the cdkey folder into the root of a USB stick (I used a 32GB FAT32 format USB stick)
* Insert into your NEOGEO-ASP, make sure the symbol appears (if it doesn't try with another correctly formatted usb stick)
* Go into options (the cog) and select System -> Upgrade