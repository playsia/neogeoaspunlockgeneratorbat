@ECHO off
SETLOCAL ENABLEEXTENSIONS

ECHO Many many thanks to cyanic for the unlocker!
ECHO This script will ask for your serial number and then generate all of the unlock keys
ECHO They can be found in keys\cdkey
ECHO After they've been generated, just move them onto your usb and unlock through the normal method!
SET /p serial="Enter your serial number (no hyphens): "
ECHO Your serial number is: %serial%
ECHO Attempting to generate the 20 unlocked games...
ECHO Using format: QuiCdkeyGen -s serial keys\cdkey\game.cdk keys\privatekey\PRIVATE_KEY.key gameid game

ECHO --------------------------
SET game=fatfury1
ECHO Game is %game%
SET gameid=20
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=fatfury2
ECHO Game is %game%
SET gameid=21
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=rbffspec
ECHO Game is %game%
SET gameid=22
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=savagere
ECHO Game is %game%
SET gameid=23
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=aof3
ECHO Game is %game%
SET gameid=24
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=lastbladh
ECHO Game is %game%
SET gameid=25
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=samshoh
ECHO Game is %game%
SET gameid=26
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslug
ECHO Game is %game%
SET gameid=27
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslug2
ECHO Game is %game%
SET gameid=28
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslug3h
ECHO Game is %game%
SET gameid=29
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslugx
ECHO Game is %game%
SET gameid=30
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslug4h
ECHO Game is %game%
SET gameid=31
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=mslug5h
ECHO Game is %game%
SET gameid=32
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

SET game=ncombat
ECHO Game is %game%
SET gameid=33
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=superspy
ECHO Game is %game%
SET gameid=34
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=shocktro
ECHO Game is %game%
SET gameid=35
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=shocktr2
ECHO Game is %game%
SET gameid=36
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=lbowling
ECHO Game is %game%
SET gameid=37
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=socbrawlh
ECHO Game is %game%
SET gameid=38
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------


SET game=ssideki
ECHO Game is %game%
SET gameid=39
ECHO ID is %gameid%
ECHO QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%
QuiCdkeyGen -s %serial% keys\cdkey\%game%.cdk keys\privatekey\PRIVATE_KEY.key %gameid% %game%

cd keys
cd cdkey

IF EXIST "%game%.cdk" (
   ECHO Success!
) ELSE (
   ECHO %game%.cdk not found...
   PAUSE
   EXIT
)

cd..
cd..

ECHO --------------------------

ECHO All 20 game unlock files have been generated!

PAUSE